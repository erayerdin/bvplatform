use bevy::prelude::{AssetServer, Commands, Res};
use bevy_ecs_ldtk::LdtkWorldBundle;

pub(super) fn load_level(mut commands: Commands, asset_server: Res<AssetServer>) {
    commands.spawn(LdtkWorldBundle {
        ldtk_handle: asset_server.load("levels.ldtk"),
        ..Default::default()
    });
}
