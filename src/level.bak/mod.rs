use bevy::prelude::{Plugin, Startup};

use self::systems::load_level;

mod systems;

pub struct LevelPlugin;

impl Plugin for LevelPlugin {
    fn build(&self, app: &mut bevy::prelude::App) {
        app.add_systems(Startup, load_level);
    }
}
