use std::time::Duration;

use bevy::{
    asset::ChangeWatcher,
    prelude::{default, App, AssetPlugin, ClearColor, Color, ImagePlugin, PluginGroup},
    DefaultPlugins,
};
use bevy_ecs_tilemap::TilemapPlugin;
use bevy_inspector_egui::quick::WorldInspectorPlugin;
use bvplatform::{LevelPlugin, RootPlugin};

fn main() {
    App::new()
        // Internal Bevy Plugins
        .insert_resource(ClearColor(
            Color::hex("333333").expect("Could not set clear color."),
        ))
        .add_plugins(
            DefaultPlugins
                .set(ImagePlugin::default_nearest())
                .set(AssetPlugin {
                    watch_for_changes: ChangeWatcher::with_delay(Duration::from_secs(1)),
                    ..default()
                }),
        )
        // 3rd-party Plugins
        // .add_plugins((LdtkPlugin, WorldInspectorPlugin::new()))
        .add_plugins((TilemapPlugin, WorldInspectorPlugin::new()))
        // Project Plugins
        .add_plugins((RootPlugin, LevelPlugin))
        // .insert_resource(LevelSelection::Index(0))
        .run();
}
