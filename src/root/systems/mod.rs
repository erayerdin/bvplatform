use bevy::prelude::{Camera2dBundle, Commands};

pub(super) fn setup(mut commands: Commands) {
    commands.spawn(Camera2dBundle::default());
}
