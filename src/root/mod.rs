use bevy::prelude::{Plugin, Startup};

use self::systems::setup;

mod systems;

pub struct RootPlugin;

impl Plugin for RootPlugin {
    fn build(&self, app: &mut bevy::prelude::App) {
        app.add_systems(Startup, setup);
    }
}
