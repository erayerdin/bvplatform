mod helpers;
mod systems;

use bevy::prelude::{Plugin, Startup};

use self::{helpers::tiled::TiledMapPlugin, systems::setup};

pub struct LevelPlugin;

impl Plugin for LevelPlugin {
    fn build(&self, app: &mut bevy::prelude::App) {
        app.add_plugins(TiledMapPlugin).add_systems(Startup, setup);
    }
}
